<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="generator" content="Asciidoctor 2.0.10">
  <meta name="author" content="John Reppy">
  <meta name="keywords" content="Standard ML, SML, SML/NJ Library">
  <title>The HashCons Library</title>
  <link rel="stylesheet" href="../styles/smlnj-lib-base.css" type="text/css" />
  <link rel="stylesheet" href="../styles/smlnj-lib.css" type="text/css" />
  <link rel="stylesheet" href="../styles/smlnj-lib-pygments.css">
  <!-- support for latexmath -->
  <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
      messageStyle: "none",
      tex2jax: {
	inlineMath: [["\\(", "\\)"]],
	displayMath: [["\\[", "\\]"]],
	ignoreClass: "nostem|nolatexmath"
      },
      asciimath2jax: {
	delimiters: [["\\$", "\\$"]],
	ignoreClass: "nostem|noasciimath"
      },
      TeX: { equationNumbers: { autoNumber: "none" } }
    })
    MathJax.Hub.Register.StartupHook("AsciiMath Jax Ready", function () {
      MathJax.InputJax.AsciiMath.postfilterHooks.Add(function (data, node) {
	if ((node = data.script.parentNode) && (node = node.parentNode) && node.classList.contains('stemblock')) {
	  data.math.root.display = "block"
	}
	return data
      })
    })
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_HTMLorMML"></script>
</head>
<body class="article">
  <div id="layout-top-mask"></div> <!-- to mask scrolled content from below -->
  <div id="layout-main">
    <div id="layout-banner-spacer"></div>
    <div id="layout-banner">
      <div id="layout-logo">
        <a href="https://smlnj.org">
          <img src="../images/smlnj-logo.png" height="120" alt="SML/NJ logo" />
        </a>
      </div> <!-- layout-logo -->
      <div id="layout-title">
	<div id="layout-title-main"><a href="../index.html">The SML of NJ Library</a></div>
	<div id="layout-title-description">The HashCons Library</div>
      </div> <!-- layout-title -->
    </div> <!-- layout-banner -->
    <div class="clearer"></div>
<div id="layout-toc">
<div id="toc">
<ul class="toc-lib-list">
  <li class="toc-lib"><a href="../Controls/controls-lib.html"><span class="toc-lib-title">The Controls Library</span></a></li>
  <li class="toc-lib"><span class="toc-lib-title" id="toc-current">The HashCons Library</span>
    <ul class="toc-page-list">
      <li class="toc-page"><a href="str-HashCons.html"><span class="toc-lib-page">The <code>HashCons</code> structure</span></a></li>
      <li class="toc-page"><a href="str-HashConsAtom.html"><span class="toc-lib-page">The <code>HashConsAtom</code> structure</span></a></li>
      <li class="toc-page"><a href="str-HashConsMap.html"><span class="toc-lib-page">The <code>HashConsMap</code> structure</span></a></li>
      <li class="toc-page"><a href="str-HashConsString.html"><span class="toc-lib-page">The <code>HashConsString</code> structure</span></a></li>
      <li class="toc-page"><a href="str-HashConsSet.html"><span class="toc-lib-page">The <code>HashConsSet</code> structure</span></a></li>
      <li class="toc-page"><a href="fun-HashConsGroundFn.html"><span class="toc-lib-page">The <code>HashConsGroundFn</code> functor</span></a></li>
    </ul>
  </li>
  <li class="toc-lib"><a href="../INet/inet-lib.html"><span class="toc-lib-title">The INet Library</span></a></li>
  <li class="toc-lib"><a href="../JSON/json-lib.html"><span class="toc-lib-title">The JSON Library</span></a></li>
  <li class="toc-lib"><a href="../Reactive/reactive-lib.html"><span class="toc-lib-title">The Reactive Library</span></a></li>
  <li class="toc-lib"><a href="../RegExp/regexp-lib.html"><span class="toc-lib-title">The RegExp Library</span></a></li>
  <li class="toc-lib"><a href="../SExp/sexp-lib.html"><span class="toc-lib-title">The SExp Library</span></a></li>
  <li class="toc-lib"><a href="../Unix/unix-lib.html"><span class="toc-lib-title">The Unix Library</span></a></li>
  <li class="toc-lib"><a href="../Util/smlnj-lib.html"><span class="toc-lib-title">The Util Library</span></a></li>
  <li class="toc-lib"><a href="../UUID/uuid-lib.html"><span class="toc-lib-title">The UUID Library</span></a></li>
  <li class="toc-lib"><a href="../XML/xml-lib.html"><span class="toc-lib-title">The XML Library</span></a></li>
</ul>
</div> <!-- toc -->
</div> <!-- layout-toc -->
    <div id="layout-content-box">
      <div id="content">
<div class="sect1">
<h2 id="_overview">Overview</h2>
<div class="sectionbody">
<div class="paragraph">
<p>The <strong>HashCons Library</strong> supports the implementation of
hash-consed representations of data structures. Such representations
are useful to reduce space usage by sharing common substructures
and to provide constant-time equality testing for large structures.</p>
</div>
<div class="paragraph">
<p>To use this library, you need to use a two-level definition of your
data structures.  For example, we might define a hash-cons representation
of lambda terms as follows:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><code data-lang="sml"><span></span><span class="tok-kr">structure</span> <span class="tok-nn">HC</span> <span class="tok-p">=</span> <span class="tok-n">HashCons</span>

<span class="tok-kr">type</span> <span class="tok-kt">var</span> <span class="tok-p">=</span> <span class="tok-nn">HashConsString</span><span class="tok-p">.</span><span class="tok-n">obj</span>

<span class="tok-kr">datatype</span> <span class="tok-kt">term_node</span>
  <span class="tok-p">=</span> <span class="tok-nc">VAR</span> <span class="tok-kr">of</span> <span class="tok-n">var</span>
  <span class="tok-p">|</span> <span class="tok-nc">LAM</span> <span class="tok-kr">of</span> <span class="tok-p">(</span><span class="tok-n">var</span> <span class="tok-n">*</span> <span class="tok-n">term</span><span class="tok-p">)</span>
  <span class="tok-p">|</span> <span class="tok-nc">APP</span> <span class="tok-kr">of</span> <span class="tok-p">(</span><span class="tok-n">term</span> <span class="tok-n">*</span> <span class="tok-n">term</span><span class="tok-p">)</span>
<span class="tok-kr">withtype</span> <span class="tok-kt">term</span> <span class="tok-p">=</span> <span class="tok-n">term_node</span> <span class="tok-nn">HC</span><span class="tok-p">.</span><span class="tok-n">obj</span></code></pre>
</div>
</div>
<div class="paragraph">
<p>And you need to define an equality function on your terms (this function
can use the hash-cons identity on subterms).  For example, here is the
equality function for our lambda terms:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><code data-lang="sml"><span></span><span class="tok-kr">fun</span> <span class="tok-nf">eq</span> <span class="tok-p">(</span><span class="tok-n">APP</span><span class="tok-p">(</span><span class="tok-n">t11</span><span class="tok-p">,</span> <span class="tok-n">t12</span><span class="tok-p">),</span> <span class="tok-n">APP</span><span class="tok-p">(</span><span class="tok-n">t21</span><span class="tok-p">,</span> <span class="tok-n">t22</span><span class="tok-p">))</span> <span class="tok-p">=</span> <span class="tok-nn">HC</span><span class="tok-p">.</span><span class="tok-n">same</span><span class="tok-p">(</span><span class="tok-n">t11</span><span class="tok-p">,</span> <span class="tok-n">t21</span><span class="tok-p">)</span> <span class="tok-kr">andalso</span> <span class="tok-nn">HC</span><span class="tok-p">.</span><span class="tok-n">same</span><span class="tok-p">(</span><span class="tok-n">t12</span><span class="tok-p">,</span> <span class="tok-n">t22</span><span class="tok-p">)</span>
  <span class="tok-p">|</span> <span class="tok-nf">eq</span> <span class="tok-p">(</span><span class="tok-n">LAM</span><span class="tok-p">(</span><span class="tok-n">x</span><span class="tok-p">,</span> <span class="tok-n">t1</span><span class="tok-p">),</span> <span class="tok-n">LAM</span><span class="tok-p">(</span><span class="tok-n">y</span><span class="tok-p">,</span> <span class="tok-n">t2</span><span class="tok-p">))</span> <span class="tok-p">=</span> <span class="tok-nn">HC</span><span class="tok-p">.</span><span class="tok-n">same</span><span class="tok-p">(</span><span class="tok-n">x</span><span class="tok-p">,</span> <span class="tok-n">y</span><span class="tok-p">)</span> <span class="tok-kr">andalso</span> <span class="tok-nn">HC</span><span class="tok-p">.</span><span class="tok-n">same</span><span class="tok-p">(</span><span class="tok-n">t1</span><span class="tok-p">,</span> <span class="tok-n">t2</span><span class="tok-p">)</span>
  <span class="tok-p">|</span> <span class="tok-nf">eq</span> <span class="tok-p">(</span><span class="tok-n">VAR</span> <span class="tok-n">x</span><span class="tok-p">,</span> <span class="tok-n">VAR</span> <span class="tok-n">y</span><span class="tok-p">)</span> <span class="tok-p">=</span> <span class="tok-nn">HC</span><span class="tok-p">.</span><span class="tok-n">same</span><span class="tok-p">(</span><span class="tok-n">x</span><span class="tok-p">,</span> <span class="tok-n">y</span><span class="tok-p">)</span>
  <span class="tok-p">|</span> <span class="tok-nf">eq</span> <span class="tok-p">_</span> <span class="tok-p">=</span> <span class="tok-n">false</span></code></pre>
</div>
</div>
<div class="paragraph">
<p>With the equality function defined, we can then create a hash-cons table:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><code data-lang="sml"><span></span><span class="tok-kr">val</span> <span class="tok-nv">tbl</span> <span class="tok-p">=</span> <span class="tok-nn">HC</span><span class="tok-p">.</span><span class="tok-n">new</span> <span class="tok-p">{</span><span class="tok-n">eq</span> <span class="tok-p">=</span> <span class="tok-n">eq</span><span class="tok-p">}</span></code></pre>
</div>
</div>
<div class="paragraph">
<p>And define constructor functions:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><code data-lang="sml"><span></span><span class="tok-kr">val</span> <span class="tok-nv">mkAPP</span> <span class="tok-p">=</span> <span class="tok-nn">HC</span><span class="tok-p">.</span><span class="tok-n">cons2</span> <span class="tok-n">tbl</span> <span class="tok-p">(</span><span class="tok-mh">0wx1</span><span class="tok-p">,</span> <span class="tok-n">APP</span><span class="tok-p">)</span>
<span class="tok-kr">val</span> <span class="tok-nv">mkLAM</span> <span class="tok-p">=</span> <span class="tok-nn">HC</span><span class="tok-p">.</span><span class="tok-n">cons2</span> <span class="tok-n">tbl</span> <span class="tok-p">(</span><span class="tok-mh">0wx3</span><span class="tok-p">,</span> <span class="tok-n">LAM</span><span class="tok-p">)</span>
<span class="tok-kr">val</span> <span class="tok-nv">mkVAR</span> <span class="tok-p">=</span> <span class="tok-nn">HC</span><span class="tok-p">.</span><span class="tok-n">cons1</span> <span class="tok-n">tbl</span> <span class="tok-p">(</span><span class="tok-mh">0wx7</span><span class="tok-p">,</span> <span class="tok-n">VAR</span><span class="tok-p">)</span>
<span class="tok-kr">val</span> <span class="tok-nv">var</span> <span class="tok-p">=</span> <span class="tok-nn">HW</span><span class="tok-p">.</span><span class="tok-n">mk</span></code></pre>
</div>
</div>
<div class="paragraph">
<p>Note that we pick successive prime numbers for the constructor hash codes.
Using these constructors, we can construct the representation of the
identity function "\(\lambda{} x . x\)" as follows:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><code data-lang="sml"><span></span><span class="tok-n">mkLAM</span><span class="tok-p">(</span><span class="tok-n">var</span> <span class="tok-s2">&quot;x&quot;</span><span class="tok-p">,</span> <span class="tok-n">mkVAR</span><span class="tok-p">(</span><span class="tok-n">var</span> <span class="tok-s2">&quot;x&quot;</span><span class="tok-p">))</span></code></pre>
</div>
</div>
<div class="paragraph">
<p>In addition to term construction, this library also supports finite sets
and maps using the unique hash-cons codes as keys.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_contents">Contents</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a href="str-HashCons.html"><code><span class="kw">structure</span> HashCons</code></a></dt>
<dd>
<p>The main module in the library, which defines the basic types
and various utility functions.</p>
</dd>
<dt class="hdlist1"><a href="str-HashConsAtom.html"><code><span class="kw">structure</span> HashConsAtom</code></a></dt>
<dd>
<p>Code to package the <a href="../Util/str-Atom.html#type:atom"><code>Atom.atom</code></a> type
as a hash-consed object.</p>
</dd>
<dt class="hdlist1"><a href="str-HashConsMap.html"><code><span class="kw">structure</span> HashConsMap</code></a></dt>
<dd>
<p>Implements finite maps keyed by hash-consed objects.</p>
</dd>
<dt class="hdlist1"><a href="str-HashConsString.html"><code><span class="kw">structure</span> HashConsString</code></a></dt>
<dd>
<p>Code to package the <code>string</code> type as a hash-consed object.</p>
</dd>
<dt class="hdlist1"><a href="str-HashConsSet.html"><code><span class="kw">structure</span> HashConsSet</code></a></dt>
<dd>
<p>Implements finite sets of hash-consed objects.</p>
</dd>
<dt class="hdlist1"><a href="fun-HashConsGroundFn.html"><code><span class="kw">functor</span> HashConsGroundFn</code></a></dt>
<dd>
<p>A functor for implementing new leaf types as hash-consed objects.</p>
</dd>
</dl>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_usage">Usage</h2>
<div class="sectionbody">
<div class="paragraph">
<p>For <a href="https://smlnj.org"><strong>SML/NJ</strong></a>, include <code>$/hash-cons-lib.cm</code> in your
<strong>CM</strong> file.</p>
</div>
<div class="paragraph">
<p>For use in <a href="https://www.mlton.org/"><strong>MLton</strong></a>, include
<code>$(SML_LIB)/smlnj-lib/HashCons/hash-cons-lib.mlb</code> in your <strong>MLB</strong> file.</p>
</div>
</div>
</div>
      </div> <!-- content -->
    </div> <!-- layout-content-box -->
    <div id="layout-footer-box">
      <div id="layout-footer">
	<div id="layout-footer-text">
	  <strong>SML/NJ</strong> Version 110.98 (August 25, 2020)<br />
	  Last updated 2020-05-20 00:54:25 UTC
	</div> <!-- layout-footer-text -->
      </div> <!-- layout-footer -->
    </div> <!-- layout-footer-box -->
  </div> <!-- layout-main -->
</body>
</html>
