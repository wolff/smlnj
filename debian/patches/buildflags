Description: Make sure the flags set by dpkg-buildflags are actually used
Author: Fabian Wolff <fabi.wolff@arcor.de>
Last-Update: 2020-11-24
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/base/runtime/objs/makefile
+++ b/base/runtime/objs/makefile
@@ -5,9 +5,9 @@
 
 SHELL =		/bin/sh
 MAKE =		make
-CC =		cc
-CPP =		/lib/cpp
-LD_LIBS =
+CC :=		cc $(CPPFLAGS) $(CFLAGS) $(LDFLAGS)
+CPP :=		/lib/cpp $(CPPFLAGS)
+LD_LIBS :=	$(LDFLAGS)
 AS =		as
 AR =		ar
 ARFLAGS =	rcv
@@ -37,7 +37,7 @@
 CLIB_DIR =	$(ROOT_DIR)/c-libs
 CONFIG_DIR =	$(ROOT_DIR)/config
 
-CFLAGS =	-O
+CFLAGS :=	-O $(CFLAGS) $(CPPFLAGS)
 
 INCLUDES =	-I$(OBJS_DIR) -I$(INC_DIR)
 GC_INCLUDES =	$(INCLUDES) -I$(GC_DIR)
@@ -292,6 +292,7 @@
 MK_ARGS =		VERSION="$(VERSION)" \
 			MAKE="$(MAKE)" \
 			CC="$(CC)" CFLAGS="$(CFLAGS)" DEFS="$(DEFS)" \
+			CPPFLAGS="$(CPPFLAGS)" LDFLAGS="$(LDFLAGS)" \
 			AR="$(AR)" ARFLAGS="$(ARFLAGS)" \
 			RANLIB="$(RANLIB)" \
 			INCLUDES="$(GC_INCLUDES) -I../bytecode"
@@ -378,6 +379,7 @@
 LIB_MK_ARGS =		VERSION="$(VERSION)" \
 			MAKE="$(MAKE)" \
 			CC="$(CC)" CFLAGS="$(CFLAGS)" DEFS="$(DEFS)" \
+			CPPFLAGS="$(CPPFLAGS)" LDFLAGS="$(LDFLAGS)" \
 			AR="$(AR)" ARFLAGS="$(ARFLAGS)" \
 			RANLIB="$(RANLIB)" \
 			INCLUDES="$(LIB_INCLUDES)"
--- a/base/runtime/objs/mk.amd64-linux
+++ b/base/runtime/objs/mk.amd64-linux
@@ -5,12 +5,12 @@
 
 MAKE =		make
 AS =		as --64
-CC =		gcc -std=gnu99 -Wall
-CFLAGS =	-O2 -m64
-CPP =		gcc -x assembler-with-cpp -E -P
+CC :=		gcc -std=gnu99 -Wall
+CFLAGS :=	-O2 -m64 $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)
+CPP :=		gcc -x assembler-with-cpp -E -P $(CPPFLAGS)
 XOBJS =
 XLIBS =		../c-libs/dl/libunix-dynload.a
-LD_LIBS =	-lm -ldl
+LD_LIBS =	-lm -ldl $(LDFLAGS)
 #CHECK_HEAP =	check-heap.o
 XDEFS =
 BASE_DEFS =
@@ -24,6 +24,6 @@
 RUNTIME_A =	run.amd64-linux.a
 
 all:
-	($(MAKE) CHECK_HEAP=$(CHECK_HEAP) RUNTIME="$(RUNTIME)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" $(RUNTIME))
-#	($(MAKE) RUNTIME="$(RUNTIME_SO)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="-shared" $(RUNTIME_SO))
-#	($(MAKE) RUNTIME_A="$(RUNTIME_A)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="" $(RUNTIME_A))
+	($(MAKE) CHECK_HEAP=$(CHECK_HEAP) RUNTIME="$(RUNTIME)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="$(LDFLAGS)" CPPFLAGS="$(CPPFLAGS)" $(RUNTIME))
+#	($(MAKE) RUNTIME="$(RUNTIME_SO)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="-shared $(LDFLAGS)" CPPFLAGS="$(CPPFLAGS)" $(RUNTIME_SO))
+#	($(MAKE) RUNTIME_A="$(RUNTIME_A)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="$(LDFLAGS)" CPPFLAGS="$(CPPFLAGS)" $(RUNTIME_A))
--- a/base/runtime/objs/mk.ppc-linux
+++ b/base/runtime/objs/mk.ppc-linux
@@ -5,15 +5,15 @@
 SHELL =		/bin/sh
 
 MAKE =		make
-CC =		gcc -ansi
-CFLAGS =	-O2
-CPP =		gcc -x c -E -P -ansi
+CC :=		gcc -ansi
+CFLAGS :=	-O2 $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)
+CPP :=		gcc -x c -E -P -ansi $(CPPFLAGS)
 #XOBJS =	xmonitor.o
 #LD_LIBS =	-lX11
 #BASE_DEFS =	-DHEAP_MONITOR
 XOBJS =
 XLIBS =
-LD_LIBS =	-lm
+LD_LIBS =	-lm $(LDFLAGS)
 XDEFS =
 BASE_DEFS =
 DEFS		= $(XDEFS) $(BASE_DEFS) -DARCH_PPC -DDSIZE_32 \
@@ -23,4 +23,4 @@
 RUNTIME =	run.ppc-linux
 
 all:
-	($(MAKE) RUNTIME="$(RUNTIME)" VERSION="$(VERSION)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" $(RUNTIME))
+	($(MAKE) RUNTIME="$(RUNTIME)" VERSION="$(VERSION)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="$(LDFLAGS)" CPPFLAGS="$(CPPFLAGS)" $(RUNTIME))
--- a/base/runtime/objs/mk.x86-linux
+++ b/base/runtime/objs/mk.x86-linux
@@ -5,15 +5,15 @@
 
 MAKE =		make
 AS =		as --32
-CC =		gcc -std=gnu99
-CFLAGS =	-O2 -m32
-CPP =		gcc -x assembler-with-cpp -E -P
+CC :=		gcc -std=gnu99
+CFLAGS :=	-O2 -m32 $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)
+CPP :=		gcc -x assembler-with-cpp -E -P $(CPPFLAGS)
 #XOBJS =	xmonitor.o
 #LD_LIBS =	-lX11
 #BASE_DEFS =	-DHEAP_MONITOR
 XOBJS =
 XLIBS =		../c-libs/dl/libunix-dynload.a
-LD_LIBS =	-lm -ldl
+LD_LIBS =	-lm -ldl $(LDFLAGS)
 XDEFS =
 BASE_DEFS =
 DEFS		= $(XDEFS) $(BASE_DEFS) -DARCH_X86 -DDSIZE_32 \
@@ -25,6 +25,6 @@
 RUNTIME_A =	run.x86-linux.a
 
 all:
-	($(MAKE) RUNTIME="$(RUNTIME)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" $(RUNTIME))
-	($(MAKE) RUNTIME="$(RUNTIME_SO)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="-shared" $(RUNTIME_SO))
-	($(MAKE) RUNTIME_A="$(RUNTIME_A)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="" $(RUNTIME_A))
+	($(MAKE) RUNTIME="$(RUNTIME)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="$(LDFLAGS)" CPPFLAGS="$(CPPFLAGS)" $(RUNTIME))
+	($(MAKE) RUNTIME="$(RUNTIME_SO)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="-shared $(LDFLAGS)" CPPFLAGS="$(CPPFLAGS)" $(RUNTIME_SO))
+	($(MAKE) RUNTIME_A="$(RUNTIME_A)" VERSION="$(VERSION)" AS="$(AS)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="$(LDFLAGS)" CPPFLAGS="$(CPPFLAGS)" $(RUNTIME_A))
--- a/base/runtime/objs/mk.x86-linux-pthreads
+++ b/base/runtime/objs/mk.x86-linux-pthreads
@@ -4,15 +4,15 @@
 SHELL =		/bin/sh
 
 MAKE =		make
-CC =		gcc -std=c99
-CFLAGS =	-O2 -D_REENTRANT
-CPP =		gcc -x assembler-with-cpp -E -P
+CC :=		gcc -std=c99
+CFLAGS :=	-O2 -D_REENTRANT $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)
+CPP :=		gcc -x assembler-with-cpp -E -P $(CPPFLAGS)
 #XOBJS =	xmonitor.o
 #LD_LIBS =	-lX11
 #BASE_DEFS =	-DHEAP_MONITOR
 XOBJS =
 XLIBS =		../c-libs/dl/libunix-dynload.a
-LD_LIBS =	-lm -ldl -lpthread
+LD_LIBS =	-lm -ldl -lpthread $(LDFLAGS)
 XDEFS =
 BASE_DEFS =
 DEFS		= $(XDEFS) $(BASE_DEFS) -DARCH_X86 -DDSIZE_32 -DOPSYS_UNIX -DOPSYS_LINUX -D_POSIX_SOURCE -D_BSD_SOURCE -DGNU_ASSEMBLER -DDLOPEN -DINDIRECT_CFUNC
@@ -21,4 +21,4 @@
 RUNTIME =	run.x86-linux
 
 all:
-	($(MAKE) RUNTIME="$(RUNTIME)" VERSION="$(VERSION)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" $(RUNTIME))
+	($(MAKE) RUNTIME="$(RUNTIME)" VERSION="$(VERSION)" CC="$(CC)" CFLAGS="$(CFLAGS)" CPP="$(CPP)" TARGET=$(TARGET) DEFS="$(DEFS)" XOBJS="$(XOBJS)" XLIBS="$(XLIBS)" LD_LIBS="$(LD_LIBS)" LDFLAGS="$(LDFLAGS)" CPPFLAGS="$(CPPFLAGS)" $(RUNTIME))
--- a/base/runtime/c-libs/makefile
+++ b/base/runtime/c-libs/makefile
@@ -4,8 +4,8 @@
 # Currently, this is only used to clean the library directories.
 #
 
-CC =		cc
-CFLAGS =	-O
+CC :=		cc
+CFLAGS :=	-O $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)
 MAKE =		make
 AR =		ar
 ARFLAGS =	rcv
--- a/base/runtime/c-libs/posix-filesys/makefile
+++ b/base/runtime/c-libs/posix-filesys/makefile
@@ -14,7 +14,7 @@
 MAKE =		make
 AR =		ar
 ARFLAGS =	rcv
-CPP =		/lib/cpp
+CPP =		/lib/cpp $(CPPFLAGS)
 RANLIB =	ranlib
 
 LIBRARY =	libposix-filesys.a
@@ -65,7 +65,7 @@
 pathconf.o : ml_pathconf.h
 
 ml_pathconf.h :
-	VERSION=$(VERSION) CPP="$(CPP)" $(CONFIG_DIR)/gen-posix-names.sh _PC_ ml_pathconf.h
+	VERSION=$(VERSION) CPP="$(CPP)" CPPFLAGS="$(CPPFLAGS)" $(CONFIG_DIR)/gen-posix-names.sh _PC_ ml_pathconf.h
 
 clean :
 	rm -f v-* *.o ml_pathconf.h $(LIBRARY)
--- a/base/runtime/c-libs/posix-procenv/makefile
+++ b/base/runtime/c-libs/posix-procenv/makefile
@@ -14,7 +14,7 @@
 MAKE =		make
 AR =		ar
 ARFLAGS =	rcv
-CPP =		/lib/cpp
+CPP =		/lib/cpp $(CPPFLAGS)
 RANLIB =	ranlib
 
 LIBRARY =	libposix-procenv.a
@@ -61,7 +61,7 @@
 sysconf.o : ml_sysconf.h
 
 ml_sysconf.h :
-	VERSION=$(VERSION) CPP="$(CPP)" $(CONFIG_DIR)/gen-posix-names.sh _SC_ ml_sysconf.h
+	VERSION=$(VERSION) CPP="$(CPP)" CPPFLAGS="$(CPPFLAGS)" $(CONFIG_DIR)/gen-posix-names.sh _SC_ ml_sysconf.h
 
 clean :
 	rm -f v-* *.o ml_sysconf.h $(LIBRARY)
